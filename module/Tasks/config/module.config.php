<?php

namespace Tasks;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Segment;

return [
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ]
    ],
    'controllers' => [
        'factories' => [
            Application\TaskController::class => Factory\TaskControllerFactory::class,
        ]
    ],
    'router' => [
        'routes' => [
            'tasks' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/tasks[/:action[/:id]]',
                    'defaults' => [
                        'controller' => Application\TaskController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'form_elements' => [
        'factories' => [
            Form\TaskForm::class => Factory\TaskFormFactory::class,
        ],
    ],
];
