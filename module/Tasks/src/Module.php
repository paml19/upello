<?php

namespace Tasks;

class Module
{
    public function getConfig()
    {
        return require(__DIR__ . '/../config/module.config.php');
    }
}
