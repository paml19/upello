<?php

namespace Tasks\Controller;

use Doctrine\ORM\EntityManager;
use Tasks\Entity\Task;
use Tasks\Form\TaskForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TaskController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var TaskForm
     */
    private $form;

    public function __construct(EntityManager $entityManager, TaskForm $form)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
    }

    public function addAction()
    {
        $id = $this->params()->fromRoute('id', null);

        if ($id) {
            try {
                $task = $this->entityManager->getRepository(Task::class)->find($id);
            } catch (\Exception $exception) {
                throw $exception;
            }

            $data = [
                'name' => $task->getName(),
            ];

            $this->form->setData($data);
        }

        $request = $this->getRequest();
        $viewModel = new ViewModel(['form' => $this->form]);

        if (! $request->isPost()) {
            return $viewModel;
        }

        $this->form->setData($request->getPost());

        if (! $this->form->isValid()) {
            return $viewModel;
        }

        try {
            $data = $this->form->getData();

            $task = (new Task)
                ->setName($data['name'])
                ->setDateAdd(new \DateTime);

            $this->entityManager->persist($task);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw $exception;
        }

        return $this->redirect()->toRoute('tasks', ['action' => 'add', 'id' => $task->getId()]);
    }

    public function listAction()
    {
        /** @var Task[] $tasks */
        $tasks = $this->entityManager->getRepository(Task::class)->findAll();

        return new ViewModel([
            'tasks' => $tasks
        ]);
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');

        try {
            $task = $this->entityManager->getRepository(Task::class)->find($id);
        } catch (\Exception $exception) {
            throw $exception;
        }

        $this->entityManager->remove($task);
        $this->entityManager->flush();

        return $this->redirect()->toRoute('tasks', ['action' => 'list']);
    }
}
