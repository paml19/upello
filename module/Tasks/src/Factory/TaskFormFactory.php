<?php

namespace Tasks\Factory;

use Interop\Container\ContainerInterface;
use Tasks\Form\TaskForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class TaskFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): TaskForm
    {
        $form = new TaskForm;

        return $form;
    }
}