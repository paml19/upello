<?php

namespace Tasks\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Tasks\Controller\TaskController;
use Tasks\Form\TaskForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class TaskControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): TaskController
    {
        return new TaskController(
            $container->get(EntityManager::class),
            $container->get('FormElementManager')->get(TaskForm::class)
        );
    }
}