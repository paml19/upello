<?php
namespace Users;
use Users\Controller\UsersController;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
'view_manager' => [
    'template_path_stack' => [
        __DIR__ . '/../view',
    ]
],
    'controllers' => [
        'factories' => [
            UsersController::class => InvokableFactory::class,
        ]
    ],
    'router' => [
        'routes' => [
            'users' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/users[/:action[/:id]]',
                    'defaults' => [
                        'controller' => UsersController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
];
?>
