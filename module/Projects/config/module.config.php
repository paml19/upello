<?php
namespace Projects;
use Projects\Controller\ProjectsController;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
'view_manager' => [
    'template_path_stack' => [
        __DIR__ . '/../view',
    ]
],
    'controllers' => [
        'factories' => [
            ProjectsController::class => InvokableFactory::class,
        ]
    ],
    'router' => [
        'routes' => [
            'users' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/projects[/:action[/:id]]',
                    'defaults' => [
                        'controller' => ProjectsController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
];
?>
